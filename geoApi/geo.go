package geoApi

import (
	"encoding/json"
	"fmt"
	"geo/config"
	"geo/models"
	"geo/restapi/operations"
	"geo/restapi/operations/ip"
	"github.com/go-redis/redis"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/go-openapi/runtime/middleware"
)

func ConfigureAPI(api *operations.GeoAPI) {

	conf, err := config.LoadConfig()
	if err != nil {
		log.Fatal(err)
	}

	opt, err := redis.ParseURL(os.Getenv("REDIS_URL"))
	if err != nil {
		log.Fatal(err)
	}

	re := redis.NewClient(&redis.Options{
		Addr:     opt.Addr,
		Password: opt.Password,
		DB:       opt.DB,
	})

	if _, err := re.Ping().Result(); err != nil {
		log.Fatal(err)
	}

	job := make(chan string)
	results := make(chan []byte)

	for _, v := range conf.Array {
		go getGeo(v.Limit, v.URL, v.Key, job, results)
	}

	api.IPIPHandler = ip.IPHandlerFunc(
		func(params ip.IPParams,
		) middleware.Responder {

			o := models.Obj{}

			splitIP := params.HTTPRequest.Header.Get("X-Forwarded-For")

			str, err := re.Get(splitIP).Result()
			if err == redis.Nil {

				job <- splitIP

				err = json.Unmarshal(<-results, &o)
				if err != nil {
					log.Fatal(err)
				}

				err := re.Set(splitIP, &o, 1*time.Hour).Err()
				if err != nil {
					log.Fatal(err)
				}

				return ip.NewIPOK().WithPayload(&o)

			} else if err != nil {

				log.Fatal(err)

			}

			err = json.Unmarshal([]byte(str), &o)
			if err != nil {
				log.Fatal(err)
			}

			return ip.NewIPOK().WithPayload(&o)

		},
	)
}

func getGeo(limit int, url, key string, job <-chan string, results chan<- []byte) {

	for paramIP := range job {

		timer := time.NewTimer(time.Second * time.Duration(limit))

		tr := &http.Transport{
			MaxIdleConns:       10,
			IdleConnTimeout:    30 * time.Second,
			DisableCompression: true,
		}

		cl := &http.Client{Transport: tr}

		resp, err := cl.Get(fmt.Sprintf("http://%s/%s?access_key=%s", url, paramIP, key))
		if err != nil {
			log.Fatal(err)
		}

		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			log.Fatal(err)
		}

		err = resp.Body.Close()
		if err != nil {
			log.Fatal(err)
		}

		results <- body

		<-timer.C

	}
}
