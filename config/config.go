package config

import (
	"encoding/json"
	"io/ioutil"
	"log"
)

// Config struct for geo
type Config struct {
	Array []struct {
		ID    int    `json:"id"`
		URL   string `json:"url"`
		Key   string `json:"key"`
		Limit int    `json:"limit"`
	} `json:"Array"`
}

// LoadConfig load configuration from file
func LoadConfig() (*Config, error) {
	b, err := ioutil.ReadFile("config.json")
	if err != nil {
		log.Fatal(err)
	}
	var conf *Config
	err = json.Unmarshal(b, &conf)
	if err != nil {
		log.Fatal(err)
	}
	return conf, err
}
