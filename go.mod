module geo

go 1.12

require (
	github.com/go-openapi/errors v0.19.0
	github.com/go-openapi/loads v0.19.0
	github.com/go-openapi/runtime v0.19.0
	github.com/go-openapi/spec v0.19.0
	github.com/go-openapi/strfmt v0.19.0
	github.com/go-openapi/swag v0.19.0
	github.com/go-redis/redis v6.15.2+incompatible
	github.com/jessevdk/go-flags v1.4.0
	golang.org/x/net v0.0.0-20190415214537-1da14a5a36f2
)
